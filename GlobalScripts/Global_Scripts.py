"""
The Global functions are reused by several test cases have been added here to facilitate code management
"""

from subprocess import call
from subprocess import Popen
import sys,os
import ctypes
import __builtin__


Start_BoostLearningIndex = False
SetGeneratorStatus = False
StopCooking = False
KeepAlive = False
GetAllPowers = False
GetCalParamCheckResult = False
GetDeviceStatus = False
GetFwVersion = False
SetGenStatusOff = False
SetGenStatusOn = False
StartFilteredLearning =False


def ConfigureSniffer():
    cmd = "msb_record | msb_filter -A | msb_format -F%%B > SnifferA.log"
    cmd = 'START "RS232Logger" ConfigureSnifferLog.bat'
    #cmd = 'ConfigureSnifferLog.bat'
    
    os.system(cmd)
    """
    DETACHED_PROCESS = 0x00000008
    cmd = [
            sys.executable,
            cmd
          ]
    p = Popen(cmd,shell=False,st    din=None,stdout=None,stderr=None,close_fds=True,creationflags=DETACHED_PROCESS)
    #call(cmd)
    """
 
 
def StateOfOven():    
    global Start_BoostLearningIndex
    global SetGeneratorStatus
    global StopCooking
    global KeepAlive
    global GetAllPowers
    global GetCalParamCheckResult
    global GetDeviceStatus
    global GetFwVersion
    global SetGenStatusOff
    global SetGenStatusOn
    global StartFilteredLearning
    
    Start_BoostLearningIndex = False
    SetGeneratorStatus = False
    StopCooking = False
    KeepAlive = False
    GetAllPowers = False
    GetCalParamCheckResult = False
    GetDeviceStatus = False
    GetFwVersion = False
    SetGenStatusOff = False
    SetGenStatusOn = False
    StartFilteredLearning = False
    
    file = open('testA.msblog', 'r')
    
    for LineItem in file:
        if "Start_BoostLearningIndex" in LineItem  > -1:
            Start_BoostLearningIndex = True
        if "SetGeneratorStatus" in LineItem > -1:
            SetGeneratorStatus = True
        if "StartCookingIndex" in LineItem > -1:
            StartCookingIndex = True    
        if "StopCooking" in LineItem > -1:
            StopCooking = True
        if "KeepAlive" in LineItem > -1:
            KeepAlive = True
        if "GetAllPowers" in LineItem > -1:
            GetAllPowers = True
        if "GetCalParamCheckResult" in LineItem > -1:
            GetCalParamCheckResult = True
        if "GetDeviceStatus" in LineItem > -1:
            GetDeviceStatus = True
        if "GetFwVersion" in LineItem > -1:
            GetFwVersion = True
        if "SetGenStatus 0 0 0 0"  in LineItem > -1:
            SetGenStatusOff = True
        if "SetGenStatus 1 1 1 1" in LineItem > -1:
            SetGenStatusOn = True
        if "StartFilteredLearning" in LineItem > -1:
            StartFilteredLearning =True


def ValidateNoLearningFrequencyLog():
    global StartFilteredLearning 
    global StartCooking
    global CookingFrequency
    
    StartFilteredLearning = False
    StartCooking = False
    CookingFrequency = False    
    
    HopCount = 0
    file = open('testA.msblog', 'r')
    
    for LineItem in file:
       
        if "StartFilteredLearning" in LineItem  > -1:
            StartFilteredLearning = True
            
            FrequencyLimits = LineItem.split( )
            
            LowFrequency  = FrequencyLimits[1]
            HighFrequency = FrequencyLimits[2]
            StepFrequency = FrequencyLimits[3]
            
            test.compare(True,True,"StartFilteredLearning found")
    test.compare(False,StartFilteredLearning, "No RF Present during testing" )
    
    
def ValidateLearningFrequencyLog():
    global StartFilteredLearning 
    global StartCooking
    global CookingFrequency
    
    StartFilteredLearning = False
    StartCooking = False
    CookingFrequency = False
    
    HopCount = 0
    file = open('testA.msblog', 'r')
    
    for LineItem in file:
        if "StartFilteredLearning" in LineItem  > -1:
            StartFilteredLearning = True
            FrequencyLimits = LineItem.split( )
            
            LowFrequency  = FrequencyLimits[1]
            HighFrequency = FrequencyLimits[2]
            StepFrequency = FrequencyLimits[3]
            
            test.compare(True,True,"StartFilteredLearning found")
            
        if "StartCooking" in LineItem > -1:
            StartCooking = True
            StartCookingFrequency = LineItem.split( )
            CookingFrequency  = StartCookingFrequency[1]
            test.compare(True,True,"StartCooking found")
            
            if (CookingFrequency <= HighFrequency) and (CookingFrequency >= LowFrequency):
                test.compare(True,True,"Frequency within Range %s (%s - %s)" %(CookingFrequency,HighFrequency,LowFrequency))
                NextFrequency = LowFrequency + (HopCount * StepFrequency)
            else:
                test.compare(False,True,"Frequency out of range %s (%s - %s)" %(CookingFrequency,HighFrequency,LowFrequency))
                
            if (CookingFrequency > LowFrequency):
                NextFrequency = CookingFrequency + StepFrequency
                test.compare(True,True,"Frequency is stepping correctly")
                
            """
            Todo
            Add test to validate that the frequencies are incrementally increasing by step size
            """       
            
def StartLog():
    #cmd= "msb_record -r start"
    #cmd = "StartSnifferLoging.bat"
    cmd = "Start StartSnifferLoging.bat"
    cmd = "startSnifferLoging.bat"
    os.system(cmd)
    """
    DETACHED_PROCES = 0x00000008
    cmd = [
            sys.executable,
            'msb_record -r start'
          ]
    p = Popen(cmd,shell=False,stdin=None,stdout=None,stderr=None,close_fds=True,creationflags=DETACHED_PROCESS)
    """
    #call('msb_record -r start')  
    # msb_format -F%B
    
    
def PauseLog():
    #cmd = "START StopSnifferLog.bat"
    #cmd = "StopSnifferLog.bat"
    cmd = "START StopSnifferLog.bat"
    cmd = "StopSnifferLog.bat"
    
    #os.system()
    os.system(cmd)
    """DETACHED_PROCESS = 0x00000008
    cmd = [
            sys.executable,
            'msb_record stop'
          ]
    p = Popen(cmd,shell=False,stdin=None,stdout=None,stderr=None,close_fds=True,creationflags=DETACHED_PROCESS)
    """
   
    
def QuitLog():
    #call('C:\ITW_Projects\IBEX Test Suites\IBEX_Regression_Testing\shared\scripts\msb_record -r quit')
    #os.system("START QuitSnifferLog.bat")
    #cmd  = "msb_record -r quit"
    #cmd = "Start StopSnifferLog.bat")
    #cmd = "StopSnifferLog.bat"
    cmd = "Start QuitSnifferLog.bat"
    cmd = "QuitSnifferLog.bat"
    os.system (cmd)
    
    """
    
    DETACHED_PROCESS = 0x00000008
    cmd = [
            sys.executable,
            'msb_record -r quit'
          ]
    p = Popen(cmd,shell=False,stdin=None,stdout=None,stderr=None,close_fds=True,creationflags=DETACHED_PROCESS)
    """   
    snooze(2)
    
    
def Formatlog():
    cmd = "START SnifferLogFormant.bat"
    cmd =  "SnifferLogFormant.bat"
    os.system(cmd)
  
    
def WaitTillRecipeStepStarts(Step):
    stepNotReached = True
    waitFor("object.exists(':Oven User Interface.recipe-steps-cooking-label_DIV')", 200)
    while stepNotReached:
        
        StepInformation = findObject(":Oven User Interface.recipe-steps-cooking-label_DIV").innerText
        Steps = StepInformation.split("Step ")
        CurrentStep = Steps[1].split("/")
        
        if (int(float(CurrentStep[0])) == int(float(Step))):
            stepNotReached = False
        snooze(2)

def ConfigureSecurityMode(UserLevel):
    """
    Goes into general settings, and configues the desired security level, and saves the settings
    """
    if UserLevel == 1:
        clickButton(waitForObject(":Oven User Interface.                             GENERAL SETTINGS                  _button"))

        # Temperature Units "F"
        mouseClick(waitForObject(":Oven User Interface.   °F   _LABEL"), 30, 31)

        
        #MManual mode
        mouseClick(waitForObject(":Oven User Interface.   OFF   _LABEL"), 26, 26)
        #Engineering mode
        #mouseClick(waitForObject(":Oven User Interface.   OFF   _LABEL_2"), 38, 31)
        clickButton(waitForObject(":Oven User Interface.SAVE _button"))
        clickButton(waitForObject(":Oven User Interface.  _button_2"))
        
        ctypes.windll.user32.MessageBoxA(0, "Please power cycle the oven", "WARNING", 1)
    
    if UserLevel == 2:
        clickButton(waitForObject(":Oven User Interface.                             GENERAL SETTINGS                  _button"))
        # Temperature Units "F"
        mouseClick(waitForObject(":Oven User Interface.   °F   _LABEL"), 30, 31)

        #MManual mode
        mouseClick(waitForObject(":Oven User Interface.   ON   _LABEL"), 26, 26)
        #Engineering mode
        #mouseClick(waitForObject(":Oven User Interface.   OFF   _LABEL_2"), 38, 31)
        clickButton(waitForObject(":Oven User Interface.SAVE _button"))
        clickButton(waitForObject(":Oven User Interface.  _button_2"))
        
    if UserLevel == 4 or UserLevel == 3:
        """
        Configure desired default oven settings
        Return: NONE
        """
        # set settings
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        mouseClick(waitForObject(":Oven User Interface.   °F   _LABEL"), 34, 22)
        mouseClick(waitForObject(":Oven User Interface.   ON   _LABEL"), 28, 32)
        mouseClick(waitForObject(":Oven User Interface.   ON   _LABEL_2"), 27, 32)
        
        # save
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.SAVE _button"))
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        
def ValidatePage(PageName,Level):
    if (PageName == "Peripherals Screen"):
            waitFor("object.exists(':Oven User Interface.BACK_button')", 20000)
            test.compare(findObject(":Oven User Interface.BACK_button").visible, True)
            test.compare(findObject(":Oven User Interface.BACK_button").id, "backToUI")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.  _button_9')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_9").domClassName, "btn btn-link btn-lg")
            test.compare(findObject(":Oven User Interface.  _button_9").visible, True)
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.  _button_10')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_10").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV2.BUTTON1")
            test.compare(findObject(":Oven User Interface.  _button_10").visible, True)
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.  _button_11')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_11").visible, True)
            test.compare(findObject(":Oven User Interface.  _button_11").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.BUTTON1")
            waitFor("object.exists(':Oven User Interface.  _button_12')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_12").visible, True)
            test.compare(findObject(":Oven User Interface.  _button_12").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV4.DIV1.BUTTON1")
            waitFor("object.exists(':Oven User Interface.  _button_13')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_13").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV4.DIV2.BUTTON1")
            test.compare(findObject(":Oven User Interface.  _button_13").visible, True)
            waitFor("object.exists(':Oven User Interface.  _button_14')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_14").visible, True)
            test.compare(findObject(":Oven User Interface.  _button_14").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV4.DIV3.BUTTON1")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            clickButton(waitForObject(":Oven User Interface.BACK_button")   )  

        
    if PageName == "Home Screen":
        if Level ==1:
            try:
                
                #activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                waitFor("object.exists(':Oven User Interface.header-label-container_DIV')", 20000)

                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            
                waitFor("object.exists(':Oven User Interface.  _button')", 200)
                test.compare(findObject(":Oven User Interface.  _button").id, "settings-button")
                waitFor("object.exists(':Oven User Interface.label-preheating_DIV')", 200)
                test.compare(findObject(":Oven User Interface.label-preheating_DIV").id, "label-preheating")
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                return True
            except:
                return False
            
        if Level ==2:
            try:
                #activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                waitFor("object.exists(':Oven User Interface.  _button_30')", 200)
                test.compare(findObject(":Oven User Interface.  _button_30").id, "home-button")
                waitFor("object.exists(':Oven User Interface.home-button_button')", 200)
                test.compare(findObject(":Oven User Interface.home-button_button").id, "header-label")
                test.compare(findObject(":Oven User Interface.home-button_button").simplifiedInnerText, "MY CREATIONS")
                waitFor("object.exists(':Oven User Interface.  _button')", 200)
                test.compare(findObject(":Oven User Interface.  _button").id, "settings-button")
                waitFor("object.exists(':Oven User Interface.label-preheating_DIV')", 200)
                test.compare(findObject(":Oven User Interface.label-preheating_DIV").id, "label-preheating")
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                waitFor("object.exists(':Oven User Interface.  _button_17')", 20000)
                test.compare(findObject(":Oven User Interface.  _button_17").id, "createNewRecipe")
                waitFor("object.exists(':Oven User Interface.  _button_26')", 20000)
                test.compare(findObject(":Oven User Interface.  _button_26").id, "editRecipe")
                waitFor("object.exists(':Oven User Interface.  _button_29')", 20000)
                test.compare(findObject(":Oven User Interface.  _button_29").id, "duplicateRecipe")
                waitFor("object.exists(':Oven User Interface.  _button_20')", 20000)
                test.compare(findObject(":Oven User Interface.  _button_20").id, "deleteRecipe")
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
           
                return True
            except:
                return False
        else:
            return False

            
def ValidateConfigurationValue(Level):
    """
    Validates that the proper configuration menu items are displayed
    based on security setting level
    """
    if Level ==2:
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        waitFor("object.exists(':{title=\\'Oven User Interface\\' type=\\'BrowserTab\\'}.DOCUMENT.HTML1.BODY1.DIV1.DIV1.DIV1.DIV5.DIV1.DIV1.DIV1.H41')", 20000)
        test.compare(findObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV1.DIV1.DIV5.DIV1.DIV1.DIV1.H41").innerText, "Settings")
        waitFor("object.exists(':Oven User Interface.general-settings-modal_DIV')", 20000)
        test.compare(findObject(":Oven User Interface.general-settings-modal_DIV").id, "buttonSettings")
        waitFor("object.exists(':Oven User Interface.                         IMPORT RECIPES         _button')", 20000)
        test.compare(findObject(":Oven User Interface.                         IMPORT RECIPES         _button").id, "buttonImportRecipes")
        waitFor("object.exists(':Oven User Interface.                           EXPORT RECIPES           _button')", 20000)
        test.compare(findObject(":Oven User Interface.                           EXPORT RECIPES           _button").simplifiedInnerText, "EXPORT RECIPES")
        test.compare(findObject(":Oven User Interface.                           EXPORT RECIPES           _button").id, "buttonExportRecipes")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    if Level ==3 or Level ==4:
        try:
            waitFor("object.exists(':Oven User Interface.general-settings-modal_DIV')", 100)
            test.compare(findObject(":Oven User Interface.general-settings-modal_DIV").simplifiedInnerText, "GENERAL SETTINGS")
            test.compare(findObject(":Oven User Interface.general-settings-modal_DIV").id, "buttonSettings")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.                         IMPORT RECIPES         _button')", 20000)
            test.compare(findObject(":Oven User Interface.                         IMPORT RECIPES         _button").simplifiedInnerText, "IMPORT RECIPES")
            test.compare(findObject(":Oven User Interface.                         IMPORT RECIPES         _button").id, "buttonImportRecipes")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.                           EXPORT RECIPES           _button')", 20000)
            test.compare(findObject(":Oven User Interface.                           EXPORT RECIPES           _button").simplifiedInnerText, "EXPORT RECIPES")
            test.compare(findObject(":Oven User Interface.                           EXPORT RECIPES           _button").id, "buttonExportRecipes")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))    
        
            waitFor("object.exists(':Oven User Interface.Service Screen icon_IMG')", 100)
            test.compare(findObject(":Oven User Interface.Service Screen icon_IMG").id, "buttonServiceScreen")
            test.compare(findObject(":Oven User Interface.Service Screen icon_IMG").simplifiedInnerText, "SERVICE SCREEN")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':{title=\\'Oven User Interface\\' type=\\'BrowserTab\\'}.DOCUMENT.HTML1.BODY1.DIV1.DIV1.DIV1.DIV5.DIV1.DIV1.DIV1.H41')", 20000)
            test.compare(findObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV1.DIV1.DIV5.DIV1.DIV1.DIV1.H41").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV1.DIV1.DIV5.DIV1.DIV1.DIV1.H41")
            test.compare(findObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV1.DIV1.DIV5.DIV1.DIV1.DIV1.H41").innerText, "Settings")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))    
        except:
            test.compare(True, False, "Test Failed in Cycle =%s" %cycle)
        
    if Level ==1: 
        #test.compare(findObject(":Oven User Interface.                           EXPORT RECIPES           _button").id, "buttonExportRecipes")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        
def StartGUI(url, UserLevel):
    """
    Function will start the browser and navigate to the desired URL
    :param url: Default URL
    :param ULEV: Specifies user level oven is at
    :return:  NONE
    """
    wrong = "\nMY CREATIONS"
    right = "MY CREATIONS"
    
    HomeButton = False

    snooze(2)
    if (UserLevel == 1):
        for index in range(15):
            startBrowser(url)
            snooze(1)
            # try
            if ValidatePage("Home Screen",1):
                status = findObject(":Oven User Interface.header-bar_DIV").innerText
            else:
                status = wrong
            if(wrong == status):
                closeWindow(":[Window]")
            else:
                break
               
    if ((UserLevel == 2) or (UserLevel == 3) or (UserLevel == 4)):
        startBrowser(url)
        snooze(.5)   
        
    if RunningOnSimulator:
        snooze(5)
        CheckForPopUp()
        
def SortRecipe(SortType):
    """
    Feature dissabled
    """
    
    """
    This function will sort the recipes as specified in parameter SortTrype
    :param SortType: Type of sort to perform
    :return:  NONE
    """
    """
    if SortType in "Date Added" > -1:
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _button_28"))
        clickLink(waitForObject(":Oven User Interface.Date Added_A"))

    if SortType in "Last Used" > -1:
        clickButton(waitForObject(":Oven User Interface.  _button_28"))
        clickLink(waitForObject(":Oven User Interface.Last Used_A"))
        # Last Used
    if SortType in "Alphabetical" > -1:
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _button_28"))
        clickLink(waitForObject(":Oven User Interface.Alphabetical_A"))
    """
    
def ExitPeripheralsScreen():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.   BACK _button"))

    
def ExitServiceScreen():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.BACK_button"))


def BackToRecipelistButton():
    # bac
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_34"))
    #exit Pop Up
    clickButton(waitForObject(":Oven User Interface.EXIT_button"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

    
def SelectServiceScreen():
    """
    Select the service screen menu
    :return: NONE
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.Service Screen icon_IMG"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))


def SelectPeripheralBeeper():
    # select beeper
    clickButton(waitForObject(":Oven User Interface.BEEPER_submit"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    snooze(1)
    # validate beeper
    waitFor("object.exists(':Oven User Interface.BEEPER_submit')", 20000)
    test.compare(findObject(":Oven User Interface.BEEPER_submit").domClassName, "col-xs-4 btn font-m btn-default")
    test.compare(findObject(":Oven User Interface.BEEPER_submit").id, "beeper-indicator-btn")
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))


def SelectGeneralSettingsScreen():
    """
    Select the General Settings Screen
    :return: NONE
    """
    clickButton(waitForObject(":Oven User Interface.  _button"))
    clickButton(waitForObject(":Oven User Interface.                             GENERAL SETTINGS                  _button"))
    snooze(2)


def SelectSoftwareFirmwareMenu():
    """
    # Select Get Software and version
    :return: NONE
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_12"))


def SelectConfigurationButton():
    """
    Selects the configuration menu
    :return:
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button"))
    

def SelectPeripherals():
    """
    Selects the Peripherals menu
    :return:
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_9"))

def SelectConvectionSettingsMenu():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_11"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))


def ValidateDefaultConvectionSettingsMenu():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.SET CATALYTIC CONVERTER WORKING TEMPERATURE_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.SET CATALYTIC CONVERTER WORKING TEMPERATURE_DIV").innerText, "SET CATALYTIC CONVERTER WORKING TEMPERATURE")
    waitFor("object.exists(':Oven User Interface.SET BLOWER INVERSION FREQUENCY_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.SET BLOWER INVERSION FREQUENCY_DIV").innerText, "SET BLOWER INVERSION FREQUENCY")
    waitFor("object.exists(':Oven User Interface.SET TEMPERATURE HYSTERESIS 15°F                     _DIV')", 20000)
    test.compare(findObject(":Oven User Interface.SET TEMPERATURE HYSTERESIS 15°F                     _DIV").simplifiedInnerText, "SET TEMPERATURE HYSTERESIS 15°F")
    waitFor("object.exists(':Oven User Interface. SAVE _button_2')", 20000)

    test.compare(findObject(":Oven User Interface.SAVE_DIV").simplifiedInnerText, "SAVE")

    
    
    waitFor("object.exists(':Oven User Interface.Convection Settings_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.Convection Settings_DIV").simplifiedInnerText, "Convection Settings")
    waitFor("object.exists(':Oven User Interface.SET CATALYTIC CONVERTER WORKING TEMPERATURE 600°F                     _DIV')", 20000)
    test.compare(findObject(":Oven User Interface.SET CATALYTIC CONVERTER WORKING TEMPERATURE_DIV").simplifiedInnerText, "SET CATALYTIC CONVERTER WORKING TEMPERATURE")
    waitFor("object.exists(':Oven User Interface.SET BLOWER INVERSION FREQUENCY 120SEC                     _DIV')", 20000)

    test.compare(findObject(":Oven User Interface.SET BLOWER INVERSION FREQUENCY_DIV").simplifiedInnerText, "SET BLOWER INVERSION FREQUENCY")


    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

    
def ValidatetConvectionSettingsMenu(CATALYTIC_CONVERTER_WORKING_TEMPERATURE, BLOWER_INVERSION_FREQUENCY, TEMPERATURE_HYSTERESIS):
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.SET CATALYTIC CONVERTER WORKING TEMPERATURE_DIV')", 20000)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
       
    current_CCWTemperature = findObject(":Oven User Interface.catalyticConvTempVal_DIV").simplifiedInnerText
        
    current_CCWTemperature = float(current_CCWTemperature.strip("°F"))
    test.compare(current_CCWTemperature, CATALYTIC_CONVERTER_WORKING_TEMPERATURE ,"Expected CCWT temperature")
    
    waitFor("object.exists(':Oven User Interface.blowerInvFreqVal_DIV')", 20000)
    current_BIF = findObject(":Oven User Interface.blowerInvFreqVal_DIV").innerText
    current_BIF = float(current_BIF.strip("SEC"))
       
    waitFor("object.exists(':Oven User Interface.SET BLOWER INVERSION FREQUENCY_DIV')", 20000)
    test.compare(current_BIF, BLOWER_INVERSION_FREQUENCY)
    

    waitFor("object.exists(':Oven User Interface.hysteresisVal_DIV')", 20000)
    CurrentBlowerHys= findObject(":Oven User Interface.hysteresisVal_DIV").innerText
    CurrentBlowerHys = float(CurrentBlowerHys.strip("°F"))
    test.compare(CurrentBlowerHys, TEMPERATURE_HYSTERESIS , "Expected Blower Hsys Temp")

    
def SelectImportRecipe():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.                         IMPORT RECIPES         _button"))
   
    
def ImportRecipe(Method =1):
    "Method =1 manual"
    "Method =2 automated"
    
    snooze(1)
    
    if Method == 1:
        Title = "Action"
        Message = "Insert USB memory Card into oven, select Import Recipe button, select recipe, and hit ok to continue"
        mb_type = 1
        MessageBox(Title, Message, mb_type)
        # import button

        
        Title = "Question"
        Message = "Select file using pop up window, and validate recipe got saved, Did Recipe get imported to USB memory?"
        mb_type = 4
        
        if  (MessageBox(Title, Message, mb_type) == "Yes"):
            return True
        else:
            return False
    else:
        "TBD"
        # import button
        
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.                         IMPORT RECIPES         _button"))
       
       
        # done
        #home Button
        clickButton(waitForObject(":Oven User Interface.  _button_2"))
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        
    
def ExportRecipe(Method =1):
    "Method =1 manual"
    "Method =2 automated"
    if Method == 1:
        Title = "Action"
        Message = "Insert USB memory Card into oven, hit ok to continue"
        mb_type = 1
        MessageBox(Title, Message, mb_type)
    

        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.                           EXPORT RECIPES           _button"))
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

        
        
        Title = "Question"
        Message = "Save file using pop up window, and validate recipe got saved, Did Recipe get exported to USB memory?"
        mb_type = 4
        
        if  (MessageBox(Title, Message, mb_type) == "Yes"):
            return True
        else:
            return False
    else:
         #Export recipe
        MessageBox(Title, Message, mb_type)
        clickButton(waitForObject(":Oven User Interface.                           EXPORT RECIPES           _button"))
    
        
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
     
     
def MessageBox(Title, Message, mb_type):
    """Raises a MessageBox dialog and returns as a string the label of the button
       pressed by the user.

       'mb_type' Code          Messagebox Type
       0                                "OK" only
       1                                "OK"/"Cancel"
       2                                "Abort"/"Retry"/"Ignore"
       3                                "Yes"/"No"/"Cancel"
       4                                "Yes"/"No"
       5                                "Retry"/"Cancel"
       6                                "Cancel"/"Try Again"/"Continue"
        :param Title: Title to appear in message  box
        :param Message: Message to display in box
        :param mb_type: Type of message to dispay
        :return: User selection
       """

    MB = ctypes.windll.user32.MessageBoxA
    returnCode = MB(None, str(Message), str(Title), mb_type)
    if returnCode == 1:
        return "OK"
    elif returnCode == 2:
        return "Cancel"
    elif returnCode == 3:
        return "Abort"
    elif returnCode == 4:
        return "Retry"
    elif returnCode == 5:
        return "Ignore"
    elif returnCode == 6:
        return "Yes"
    elif returnCode == 7:
        return "No"
    elif returnCode == 10:
        return "Try Again"
    elif returnCode == 11:
        return "Continue"
    else:
        if mb_type < 0 or mb_type > 6:
            raise Exception("Parameter for argument 'mb_type' is invalid. " \
                        "Parameter must be a value in range of 0:7")


def ExtendedTime(ExtendedCookingTime):
    """
    press extended button
    :param ExtendedCookingTime:
    :return: NONE
    """
    

    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_5"))
    
    # check menu extention time to 30
    waitFor("object.exists(':Oven User Interface.               --°F          Do you really wantto cook more?                        Sub message        Main message second          CANCEL      30 secs      60 secs     _DIV')", 20000)
    test.compare(findObject(":Oven User Interface.               --°F          Do you really wantto cook more?                        Sub message        Main message second          CANCEL      30 secs      60 secs     _DIV").domPath, "DOCUMENT.HTML1.BODY1.DIV3.DIV1.DIV1.DIV1")
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab")) 
    
    if ExtendedCookingTime == 60:
        clickButton(waitForObject(":Oven User Interface.60 secs_button"))

    else :
        clickButton(waitForObject(":Oven User Interface.30 secs_button"))
    
    waitFor("object.exists(':Oven User Interface.manualModalRemainingLabel_DIV')", 200)
   
def SetConfigurationToDegreeC():
    mouseClick(waitForObject(":Oven User Interface.   °C   _LABEL"), 36, 35)
    clickButton(waitForObject(":Oven User Interface.Save _button"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

    
def SetConfigurationToDegreeF():
 
    mouseClick(waitForObject(":Oven User Interface.   °F   _LABEL"), 27, 31)
    clickButton(waitForObject(":Oven User Interface.Save _button"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

    
def SelectBakeryMenu():
    """
    Selects the bakery menu
    :return: NONE
    """
    """
    if object.exists(':Oven User Interface.  _button_30'):
  
        waitFor("object.exists(':Oven User Interface.  _button_30')", 20000)
        test.compare(findObject(":Oven User Interface.  _button_30").id, "home-button")
        test.compare(findObject(":Oven User Interface.  _button_30").domClassName, "btn btn-link-def display-level-1")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        waitFor("object.exists(':Oven User Interface.TEMP_DIV')", 20000)
        test.compare(findObject(":Oven User Interface.TEMP_DIV").innerText, "TEMP")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    else:
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickLink(waitForObject(":Oven User Interface.Bakery_AREA_2"))

    """
    try:
        RecipePageFound = findObject(":Oven User Interface.home-button_button").innerText 
    
        if RecipePageFound == "MY CREATIONS":   
            mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV1.DIV1.DIV1.BUTTON1.IMG1"), 14, 15)

    except:
        RecipePageFound = "Already bakerey page"


def SelectDelMenu():
    """
    Selects the Delete Button from menu
    :return: NONE
    """
    # Delete Menu validated for user 2
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_45"))



def RecipeScrollUp():
    mouseClick(waitForObject(":Oven User Interface.recipe-list_DIV"), 1165, 10)



def RecipeScrollDown():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    mouseClick(waitForObject(":Oven User Interface.recipe-list_DIV"), 1160, 302)
    # sel first
   
    
def RecipeEmpty():
    """
    Specicied if recipe list is empty
    :return: Recipe existance status
    """
    

    if waitFor("object.exists(':Oven User Interface.recipe-list-item-0_DIV')", 200):
        # messagePopUp("Recipes Found")
        Status = False
    else:
        # messagePopUp("Recipes not Found")
        Status = True

    
   # MessageBox("Recipe empty",Status,1)
    return Status


def CheckForPopUp():
    snooze(5)
    try:
        # check for Passworkd Pop Up
        PopUpExist = findObject(":Oven User Interface.Enter Code_DIV").visible
        if PopUpExist:
            status = "PSW"
        else:
            status = "None"
    except:
        status =  "None"
    
    try:
        #Check for issue with hardware pop up window
        PopUpExist = findObject(":Oven User Interface.I/O Communication Error_DIV").innerText
        if "I/O Communication Error" in  PopUpExist > -1:
            status =  "I/O Pop Up Exists"
        else:
            status =  None
        waitFor("object.exists(':Oven User Interface.I/O Communication Error_DIV')", 20000)
        test.compare(findObject(":Oven User Interface.I/O Communication Error_DIV").innerText, "I/O Communication Error")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        
        # Hit skip button
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.SKIP_button"))
        status =  "I/O Error"
    except:
        status = "None"
        
    if status == "None":
        return False
    else:
        return True


def EnterWrongPassword():
    
        clickButton(waitForObject(":Oven User Interface.1_button"))
        clickButton(waitForObject(":Oven User Interface.1_button"))
        clickButton(waitForObject(":Oven User Interface.1_button"))
        clickButton(waitForObject(":Oven User Interface.1_button"))
        clickButton(waitForObject(":Oven User Interface.OK_DIV"))
    
        snooze(2)
        test.compare(findObject(":Oven User Interface.Wrong Password.Try again._DIV").innerText, "Wrong Password.Try again.")
        test.compare(findObject(":Oven User Interface.Wrong Password.Try again._DIV").domClassName, "col-xs-12 main-msg")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.OK_button_3"))    
        
    
def CheckPasswordPopUp(UserLevel):
    # check for PSW pop up
    try:
        PopUpExist = findObject(":Oven User Interface.Enter Code_DIV").visible
        if PopUpExist:
            # enter password
            if UserLevel == 1:
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.CANCEL_DIV"))
                # cancel button
                clickButton(waitForObject(":Oven User Interface.CANCEL _button"))

                
                
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
      
            if UserLevel == 4:
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.1_button"))
                clickButton(waitForObject(":Oven User Interface.9_button"))
                clickButton(waitForObject(":Oven User Interface.9_button"))
                clickButton(waitForObject(":Oven User Interface.2_button"))
                clickButton(waitForObject(":Oven User Interface.OK_DIV"))
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
      
                
            if UserLevel == 2:
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.1_button"))
                clickButton(waitForObject(":Oven User Interface.0_button"))
                clickButton(waitForObject(":Oven User Interface.5_button"))
                clickButton(waitForObject(":Oven User Interface.1_button"))
                clickButton(waitForObject(":Oven User Interface.OK_DIV"))
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                
            if UserLevel == 3:
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.6_button"))
                clickButton(waitForObject(":Oven User Interface.7_button"))
                clickButton(waitForObject(":Oven User Interface.3_button"))
                clickButton(waitForObject(":Oven User Interface.1_button"))
                clickButton(waitForObject(":Oven User Interface.OK_DIV"))
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    except:
        snooze(.1)
        
        
def RecipeListEmpty():
    if waitFor("object.exists(':Oven User Interface.recipe-list-item-0_DIV')", 200):
        #messagePopUp("Recipes Found")
        return False
    else:
        #messagePopUp("Recipes not Found")
        return True
    

def GetNameOfFirstRecipe():
    CurrentRecipeName = findObject(":Oven User Interface.recipe-list-item-0_DIV").simplifiedInnerText
    return CurrentRecipeName


def DeleteAllRecipes():
    
    items_Deleted = 0
    
    for i in xrange(RecipeCountLimit):
        if RecipeEmpty():
            break
        else:
            snooze(1)
            clickButton(waitForObject(":Oven User Interface.  _button_19"))
            
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            clickButton(waitForObject(":Oven User Interface.  DELETE _button"))
            clickButton(waitForObject(":Oven User Interface.OK_button_5"))
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            
            #clickButton(waitForObject(":Oven User Interface.  _button_2"))
            items_Deleted = items_Deleted+1
            snooze(2)
    return items_Deleted
        
                
def DeleteRecipe(RecipeName):
    """
    Deletes selected recipe
    :param RecipeName:
    :return: NONE
    """
    items_Deleted = 0
    global items_Deleted
    found = 0
    
    if RecipeEmpty():
        found = 0
        # messagePopUp("No Recipes")
    else:
        # messagePopUp("Recipes Found")

        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        waitFor("object.exists(':Oven User Interface.recipe-list_DIV')", 20000)
        RecipeItems = (findObject(":Oven User Interface.recipe-list_DIV").simplifiedInnerText).split(" ")

        
        waitFor("object.exists(':Oven User Interface.recipe-list_DIV')", 20000)
        RawRecipeList = findObject(":Oven User Interface.recipe-list_DIV").innerText
        RawRecipeList = RawRecipeList.replace("\t", "")
        RawRecipeList = RawRecipeList.replace("  ", "")
        RawRecipeList = RawRecipeList.replace("\r", "")
        RawRecipeList.rstrip("\n")
        list = RawRecipeList.split("\n")
                
        index = 0
        for CurrentRecipe in list:
            if len(CurrentRecipe) > 1: 
                                  
                if RecipeName == CurrentRecipe:
                    test.compare(CurrentRecipe, RecipeName)
 
                    
                    # if test.verify("object.exists(':Oven User Interface.recipe-list-item-%s_DIV' )" %index):
                    found = found + 1
                       
                    snooze(3)
 
                    # mouseClick(waitForObject(":Oven User Interface. %s _DIV" %RecipeName),200)
                    clickButton(waitForObject(":Oven User Interface.recipe-list-item-%s_DIV" % index))
                    snooze(1)

                    clickButton(waitForObject(":Oven User Interface.  DELETE _button"))
                    
                     # ok delete button
                    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                    clickButton(waitForObject(":Oven User Interface.OK_button_5"))

                    
                    clickButton(waitForObject(":Oven User Interface.  _button_2"))
                    items_Deleted = items_Deleted + 1
                    snooze(1)
                    break
                index = index + 1
    test.compare(items_Deleted >= 1, True, "one or more Recipes deleted")
    return items_Deleted


def DoesRecipeExist(RecipeName):
    """
    Checks if  selected recipe exists
    :param RecipeName:
    :return: True
    """
    items_found = 0
    global items_found
    found = 0
    
    if RecipeEmpty():
        found = 0
        # messagePopUp("No Recipes")
    else:
        # messagePopUp("Recipes Found")

        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        waitFor("object.exists(':Oven User Interface.recipe-list_DIV')", 20000)
        RecipeItems = (findObject(":Oven User Interface.recipe-list_DIV").simplifiedInnerText).split(" ")

        
        waitFor("object.exists(':Oven User Interface.recipe-list_DIV')", 20000)
        RawRecipeList = findObject(":Oven User Interface.recipe-list_DIV").innerText
        RawRecipeList = RawRecipeList.replace("\t", "")
        RawRecipeList = RawRecipeList.replace("  ", "")
        RawRecipeList = RawRecipeList.replace("\r", "")
        RawRecipeList.rstrip("\n")
        list = RawRecipeList.split("\n")
                
        index = 0
        for CurrentRecipe in list:
            if len(CurrentRecipe) > 1: 
                                  
                if RecipeName == CurrentRecipe:
                    test.compare(CurrentRecipe, RecipeName)
 
                    found = found + 1                
                    snooze(3)
                    items_found = items_found + 1
                    snooze(1)
                    break
                index = index + 1
   
    if items_found >=1:
        return True
    else:
        return False
  
  
def CheckRecipesPageLooksRight():
    for i in range(10):
        waitFor("object.exists(':Oven User Interface.recipe-buttons-bar_DIV')", 200)
        try:
            waitFor("object.exists(':Oven User Interface.recipe-buttons-bar_DIV')", 200)
            SIT = findObject(":Oven User Interface.recipe-buttons-bar_DIV").visible
            test.compare(findObject(":Oven User Interface.recipe-buttons-bar_DIV").visible, True)
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.recipe-start-button_DIV')", 200)
            test.compare(findObject(":Oven User Interface.recipe-start-button_DIV").simplifiedInnerText, "START")
            test.compare(findObject(":Oven User Interface.recipe-start-button_DIV").visible, True)
            test.compare(findObject(":Oven User Interface.recipe-start-button_DIV").id, "recipe-start-button")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            break
        
        except:
            ClickHomeButton()
            SelectRecipeMenu()        
                    
def ValidateRecipeMenuQuick(Level): 
    try:

        # valildate Preheat label
        try:
            test.compare(findObject(":Oven User Interface.label-preheating_DIV").id, "label-preheating")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        except:
            test.compare(False,False,"Preheat Button Not Found")
            return False
        
        
        if not(Level == 1):
            waitFor("object.exists(':Oven User Interface.home-button_button')", 100)
            test.compare(findObject(":Oven User Interface.home-button_button").id, "header-label")
            test.compare(findObject(":Oven User Interface.home-button_button").innerText, "MY CREATIONS")
        
        waitFor("object.exists(':Oven User Interface.  _button')", 100)
        test.compare(findObject(":Oven User Interface.  _button").domClassName, "btn btn-link-def")
        test.compare(findObject(":Oven User Interface.  _button").id, "settings-button")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    
      
        
        # Create new recipe +
        try: 
            
            findObject(":Oven User Interface.  _button_17").visible
        except:
            test.compare(False,False,"New recipe  Button Not Found")
            return False
        
        #Edit button
        try:
            waitFor("object.exists(':Oven User Interface.  _button_26')", 100)
            findObject(":Oven User Interface.  _button_26").visible
        except:
            test.compare(False,False,"Edit Button Not Found")
            return False
        # duplicate
        try:
            waitFor("object.exists(':Oven User Interface.  _button_29')", 100)
            findObject(":Oven User Interface.  _button_29").visible
        except:
            test.compare(False,False,"Duplicate Button Not Found")
            return False
            
        #validate delete
        try:
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            findObject(":Oven User Interface.  _button_45").visible
        except:
            test.compare(False,False,"Delete Button Not Found")
            return False


        try:
            #Start Button
            waitFor("object.exists(':Oven User Interface.recipe-start-button_DIV')", 100)
            findObject(":Oven User Interface.recipe-start-button_DIV").visible

        except:
            test.compare(False,False,"Start Button Not Found")
            return False
        
        return True
    except:
        test.compare(False,False, "Recipe menu not correct")
        return False          
     
def ValidateRecipeMenu(Level):
    try:

        # valildate Preheat label
        try:
            test.compare(findObject(":Oven User Interface.label-preheating_DIV").id, "label-preheating")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        except:
            test.compare(True,False,"Preheat Button Not Found")
            return False
        
        
        if not(Level == 1):
            waitFor("object.exists(':Oven User Interface.home-button_button')", 100)
            test.compare(findObject(":Oven User Interface.home-button_button").id, "header-label")
            test.compare(findObject(":Oven User Interface.home-button_button").innerText, "MY CREATIONS")
        
        waitFor("object.exists(':Oven User Interface.  _button')", 100)
        test.compare(findObject(":Oven User Interface.  _button").domClassName, "btn btn-link-def")
        test.compare(findObject(":Oven User Interface.  _button").id, "settings-button")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    
      
        
        # Create new recipe +
        try: 
            
            test.compare(findObject(":Oven User Interface.  _button_17").visible,True)
        except:
            test.compare(False,False,"New recipe  Button Not Found")
            return False
        
        #Edit button
        try:
            waitFor("object.exists(':Oven User Interface.  _button_26')", 100)
            test.compare(findObject(":Oven User Interface.  _button_26").visible, True)
        except:
            test.compare(False,False,"Edit Button Not Found")
            return False
        # duplicate
        try:
            waitFor("object.exists(':Oven User Interface.  _button_29')", 100)
            test.compare(findObject(":Oven User Interface.  _button_29").visible, True)
        except:
            test.compare(True,False,"Duplicate Button Not Found")
            return False
            
        #validate delete
        try:
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            test.compare(findObject(":Oven User Interface.  _button_45").visible, True)
        except:
            test.compare(True,False,"Delete Button Not Found")
            return False


        try:
            #Start Button
            waitFor("object.exists(':Oven User Interface.recipe-start-button_DIV')", 100)
            test.compare(findObject(":Oven User Interface.recipe-start-button_DIV").visible, True)

        except:
            test.compare(True,False,"Start Button Not Found")
            return False
        
        return True
    except:
        test.compare(True,False, "Recipe menu correct")
        return False
    
def SelectRecipeMenu(ULevel=1):
    """
    Selects the recipe menu
    :param ULevel: Assumes lev 1 user by default
    :return: NONE
    """
    
    for loopindex in range(10):
        if (ULevel == 1):
            # Do nothing
            Done=True
            break
        else:
            
            snooze(1)
                    
            ActivePage = GetActivePageName()
                   #MessageBox("Action", ActivePage, 1)
            if ActivePage == "Manual":
                # Press homeButton
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.  _button_30"))
                snooze(2)
                        
                RecipeMenuIsAccurate = ValidateRecipeMenuQuick(ULevel)
                if RecipeMenuIsAccurate:
                    break
                else:
                    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                    clickButton(waitForObject(":Oven User Interface.  _button_30"))
                    snooze(2)

    snooze(1)
    
    
def GetActivePageName():
    """
    Checks to see in main page is shown or recipe page
    """
    
    ActivePage = "Unkonw"
    snooze(2)
    waitFor("object.exists(':Oven User Interface.  _button_30')", 200)
    try:
        HomeButtonText = findObject(":Oven User Interface.  _button_30").innerHTML
    except:
        HomeButtonText="none"
        
    #MessageBox ("Action","Home menu text =%s" %HomeButtonText,1  )

    if "Recipe.png" in HomeButtonText > -1:
        ActivePage  = "Manual"
        
    if  "\n			<img src=\"images/Manual.png\" style=\"width: 40px;\" data-holder-rendered=\"true\">" in HomeButtonText > -1:
        #activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        ActivePage = "Recipe"

    return ActivePage
    

def goto_self_check():
    snooze(1)
    clickButton(waitForObject(":Oven User Interface.  _button_14"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    
def goto_serviceScreen():

    StartGUI(url,UserLevel)

    snooze(1)
    clickLink(waitForObject(":Oven User Interface.Recipes_AREA"))
    clickButton(waitForObject(":Oven User Interface.  _button"))
    clickButton(waitForObject(":Oven User Interface.                             SERVICE SCREEN           _button"))    

def check_io_state():

    # check
    waitFor("object.exists(':Oven User Interface.    I/O TEST   _DIV')", 20000)
    test.compare(findObject(":Oven User Interface.    I/O TEST   _DIV").visible, True)
    test.compare(findObject(":Oven User Interface.    I/O TEST   _DIV").innerHTML, "\n        <button type=\"button\" class=\"btn btn-success\" id=\"btnIOTest\" style=\"font-size: 3.0vmin; font-weight: bold; padding-top: 13px; padding-bottom: 13px;\" disabled=\"disabled\">\n            <span class=\"glyphicon glyphicon-ok\">&nbsp;</span>I/O TEST\n        </button>")

    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))    
    
def check_Rf_State():
    
    # check RF state
    waitFor("object.exists(':Oven User Interface.    RF TEST   _DIV')", 20000)
    test.compare(findObject(":Oven User Interface.    RF TEST   _DIV").innerHTML, "\n        <button type=\"button\" class=\"btn btn-success\" id=\"btnRFTest\" style=\"font-size: 3.0vmin; font-weight: bold; padding-top: 13px; padding-bottom: 13px;\" disabled=\"disabled\">\n            <span class=\"glyphicon glyphicon-ok\">&nbsp;</span>RF TEST\n        </button>")
    test.compare(findObject(":Oven User Interface.    RF TEST   _DIV").visible, True)


    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_15"))
    clickButton(waitForObject(":Oven User Interface.   BACK _button_2"))


      
        
def GotoHomeScreen():
    """
    Selects home screen button
    :return: None
    """

    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_2"))

    
def CheckRecipeDone():
    Cooking = True
    
    while Cooking:
       #TimeLeft=GetTimerValue()
        snooze(2)
        Timer = findObject(":Oven User Interface.manualModalRemainingLabel_DIV").simplifiedInnerText
        
        if ("00:00:00" == Timer):
            Cooking = False
            break
        snooze(1)
        
        
def StartRecipeCooking():
    """
    Select start cooking from recipe menu
    :return: NONE
    """
    try:
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface. START _button"))  
    except:    
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface. START _button"))   
def StartManualCooking():
    """
    Presses Start Button for manual cooking
    :return: NONE
"""
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
#    clickButton(waitForObject(":Oven User Interface. START _button"))
  

    clickButton(waitForObject(":Oven User Interface.Recipes_AREA_3"))


           
def StartCooking():
    """
    Presses Start Button for manual cooking
    :return: NONE
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface. START _button"))
    
    
def SelectReheatMenu():
    """
    Selects reheat menu
    :return: NONE
    """
    clickLink(waitForObject(":Oven User Interface.Reheating_AREA"))
      
      
def SelectLiquidsMenu():
    """
    Selects liquids menu
    :return: NONE
    """
    clickLink(waitForObject(":Oven User Interface.Liquids_AREA")) 
     
     
def SelectProteinsMenu():
    """
    Selects Proteins menu
    :return: NONE
    """
    clickLink(waitForObject(":Oven User Interface.Proteins_AREA"))
    
    
def SetBakingSpecs(CookingTemp, RF_Energy, FanSpeed, CookingTime) :
        """
        Sets the cooking specifications
        :param CookingTemp: Desired cooking Temperature
        :param RF_Energy: desired the Energy level
        :param FanSpeed: desired fan speed
        :param CookingTime: Desired CookingTime
        :return: NONE
        """
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        mouseClick(waitForObject(":Oven User Interface.progress-bar-temp_DIV"), 155, 17)
        mouseClick(waitForObject(":Oven User Interface.progress-bar-temp_DIV"), 132, 21)
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        mouseClick(waitForObject(":Oven User Interface.LOW_LABEL"), 84, 9)
        mouseClick(waitForObject(":Oven User Interface.LOW_LABEL"), 85, 3)
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        mouseClick(waitForObject(":Oven User Interface.LOWMEDIUMHIGH_DIV"), 193, 14)
        mouseClick(waitForObject(":Oven User Interface.OFF_LABEL"), 90, 11)
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        mouseClick(waitForObject(":Oven User Interface.progress-bar-time_DIV"), 43, 19)
        mouseClick(waitForObject(":Oven User Interface.010'0020'00_DIV"), 57, 20)
        mouseClick(waitForObject(":Oven User Interface.010'0020'00_DIV"), 68, 22)
        mouseClick(waitForObject(":Oven User Interface.progress-bar-time_DIV"), 61, 24)
        mouseClick(waitForObject(":Oven User Interface.progress-bar-time_DIV"), 57, 24)
        mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV2.DIV2.DIV2.DIV1.DIV2"), 5, 29)
        mouseClick(waitForObject(":Oven User Interface.progress-bar-time_DIV"), 58, 24)
        mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV2.DIV2.DIV2.DIV1.DIV2"), 1, 30)
        mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV2.DIV2.DIV2.DIV1.DIV2"), 1, 31)
        mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV2.DIV2.DIV2.DIV1.DIV2"), 4, 32)
        mouseClick(waitForObject(":Oven User Interface.progress-bar-time_DIV"), 56, 28)
        clickButton(waitForObject(":Oven User Interface.  _button_3"))
        clickButton(waitForObject(":Oven User Interface.  _button_3"))


def SetCookingTemp(CookingTemp):
    """
    Sets the Cooking Temperature
    :param CookingTemp: Desired cooking temperature to set
    :return: NONE
    """
    current_temp = ""
    setTemp_completed = False    
    snooze(1)
    while not (setTemp_completed): 
 
        #waitForObject(":Oven User Interface.progress-label-temp_DIV",200)
        current_temp = findObject(":Oven User Interface.progress-label-temp_DIV").simplifiedInnerText
        # test.compare(current_temp, "%s°F" %CookingTemp)
        
        # test.compare(current_temp, CookingTemp, "Adjusting Temp")
        if (current_temp == "%s°F" % CookingTemp):
            setTemp_completed = True
        
        else:
            ct = current_temp.strip("°F")
            ct = float(ct)
            
            if CookingTemp > ct:
                clickButton(waitForObject(":Oven User Interface.  _button_8"))  # Increase temp
            if CookingTemp < ct:
                clickButton(waitForObject(":Oven User Interface.  _button_7"))  # decrease temp


def SetRFEnergy(EnergyLevel):
    """
    Set the desired RF energy level
    :param EnergyLevel:
    :return: NONE
    """
    current_Energy = float(0)
    setTarget_completed = False
    count = 0
    while not (setTarget_completed) or count > 20:   
        count = count + 1
        
        current_Energy = findObject(":Oven User Interface.progress-label-rflevel_DIV").simplifiedInnerText
        current_Energy = float(current_Energy)
        
        if (current_Energy == EnergyLevel):
            setTarget_completed = True
        else:
            ce = float(current_Energy)
            int(ce)
            
            if EnergyLevel > ce:
                clickButton(waitForObject(":Oven User Interface.  _button_21"))
            if EnergyLevel < ce:
                clickButton(waitForObject(":Oven User Interface.  _button_22"))


def ValidatePeripheralBlowerOff(Direction = "ClockWise"):
    if Direction == "ClockWise":
        # check blower is off
        waitFor("object.exists(':Oven User Interface.blowerValActual_DIV')", 20000)
    
        test.compare(findObject(":Oven User Interface.blowerValActual_DIV").id, "blowerValActual")
        test.compare(findObject(":Oven User Interface.blowerValActual_DIV").simplifiedInnerText, "Blower: not running VFD: OK (Eaton)")
        #activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    else:

        # blower turn off validation
        waitFor("object.exists(':Oven User Interface.  _submit_3')", 20000)
        test.compare(findObject(":Oven User Interface.  _submit_3").domClassName, "btn font-m btn-default")
        test.compare(findObject(":Oven User Interface.  _submit_3").id, "blowerCcw-indicator-btn")
        test.compare(findObject(":Oven User Interface.  _submit_3").innerHTML, "\n				<img src=\"images/counterclockwise-icon.png\" data-holder-rendered=\"true\" alt=\"Bccw\" style=\"height: 40px;\">")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

   

    
    
def ValidatePeripheralBlowerOn(Direction = "ClockWise"):
    if Direction == "ClockWise":
        waitFor("object.exists(':Oven User Interface.blowerValActual_DIV')", 20000)
        test.compare(findObject(":Oven User Interface.blowerValActual_DIV").id, "blowerValActual")
        test.compare(findObject(":Oven User Interface.blowerValActual_DIV").visible, True)
        test.compare(findObject(":Oven User Interface.blowerValActual_DIV").simplifiedInnerText, "Blower: running VFD: OK (Eaton)")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    else:
        # clockwise on
        waitFor("object.exists(':Oven User Interface.  _submit_3')", 20000)
        test.compare(findObject(":Oven User Interface.  _submit_3").domClassName, "btn font-m btn-success")
        test.compare(findObject(":Oven User Interface.  _submit_3").id, "blowerCcw-indicator-btn")
        test.compare(findObject(":Oven User Interface.  _submit_3").innerHTML, "\n				<img src=\"images/counterclockwise-icon.png\" data-holder-rendered=\"true\" alt=\"Bccw\" style=\"height: 40px;\">")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

        
def SetPeripheralBlowerOn(Direction = "ClockWise"):
    # Power On
    #activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    #clickButton(waitForObject(":Oven User Interface.BLOWER_submit"))

    snooze(9)

    if Direction == "ClockWise":
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _submit_2"))
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        # validate
        
        snooze(5)
        waitFor("object.exists(':Oven User Interface.blowerValActual_DIV')", 20000)
        test.compare(findObject(":Oven User Interface.blowerValActual_DIV").id, "blowerValActual")
        test.compare(findObject(":Oven User Interface.blowerValActual_DIV").simplifiedInnerText, "Blower: running VFD: OK (Eaton)")
        waitFor("object.exists(':Oven User Interface.  _submit_2')", 20000)
        test.compare(findObject(":Oven User Interface.  _submit_2").domClassName, "btn font-m btn-success")
        test.compare(findObject(":Oven User Interface.  _submit_2").id, "blower-indicator-btn")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    else:
        
        # Fan counter clock wise on
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _submit_3"))
        waitFor("object.exists(':Oven User Interface.  _submit_3')", 20000)
        snooze(5)

        test.compare(findObject(":Oven User Interface.  _submit_3").domClassName, "btn font-m btn-success")
        test.compare(findObject(":Oven User Interface.  _submit_3").innerHTML, "\n				<img src=\"images/counterclockwise-icon.png\" data-holder-rendered=\"true\" alt=\"Bccw\" style=\"height: 40px;\">")
   
    ValidatePeripheralBlowerOn(Direction)
    

def SetPeripheralBlowerOff(Direction = "ClockWise"):
    # Power Off
    if Direction == "ClockWise":
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _submit_2"))
        snooze(9)
        ValidatePeripheralBlowerOff()
    else:
        # counter clock wise off
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _submit_3"))

    snooze(9)    
    ValidatePeripheralBlowerOff(Direction)
    
    
def SetPeripheralsRFEnergy(EnergyLevel):
    """
    Set the desired RF energy level
    :param EnergyLevel:
    :return: NONE
    """
    current_Energy = float(0)
    setTarget_completed = False
    count = 0
    while not (setTarget_completed) or count > 20:   
        count = count + 1
        
        current_Energy = findObject(":Oven User Interface.rfLevelVal_DIV").simplifiedInnerText
        current_Energy = float(current_Energy)
        
        if (current_Energy == EnergyLevel):
            setTarget_completed = True
        else:
            ce = float(current_Energy)
            int(ce)
            
            if EnergyLevel > ce:
                # RF Right Arrow
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.      _button_8"))
                                
            if EnergyLevel < ce:

                 # RF leff arrow
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.      _button_7"))
                
             # energy level
            test.compare(findObject(":Oven User Interface.rfLevelVal_DIV").simplifiedInnerText, "%s" %EnergyLevel)

def SetConvectionCCWT(CCWTemperature):
    """
    Set the desired Temperature level
    :param Temperaturel:
    :return: NONE
    """
    current_Energy = float(0)
    setTarget_completed = False
    count = 0
    while not (setTarget_completed) or count > 20:   
        count = count + 1


        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
       
        current_CCWTemperature = findObject(":Oven User Interface.catalyticConvTempVal_DIV").simplifiedInnerText
        
        current_CCWTemperature = float(current_CCWTemperature.strip("°F"))
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))


        
        if (current_CCWTemperature == CCWTemperature):
            setTarget_completed = True
        else:
            ce = float(current_CCWTemperature)
            int(ce)
            
            if CCWTemperature > ce:
                # RF Right Arrow
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.  _button_38"))

            if CCWTemperature < ce:

                 # RF leff arrow
                 activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                 clickButton(waitForObject(":Oven User Interface.  _button_39"))

                 
    # energy level
    current_CCWTemperature = findObject(":Oven User Interface.catalyticConvTempVal_DIV").simplifiedInnerText
        
    current_CCWTemperature = float(current_CCWTemperature.strip("°F"))
    test.compare(current_CCWTemperature, CCWTemperature,"Target Reached")


def SetConvectionTempHys(TempHys):
    """
    Set the desired Blower Inverce Frequency
    :param EnergyLevel:
    :return: NONE
    """
    current_TempHys = float(0)
    setTarget_completed = False
    count = 0
    while not (setTarget_completed) or count > 20:   
        count = count + 1
        current_TempHys = findObject(":Oven User Interface.hysteresisVal_DIV").innerText

        current_TempHys = float(current_TempHys.strip("°F"))
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))


        
        if (current_TempHys == TempHys):
            setTarget_completed = True
        else:
            ce = float(current_TempHys)
            int(ce)
            
            if TempHys > ce:
                # arrow right
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.  _button_42"))

                
            if TempHys < ce:
                # arrow Left
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.  _button_43"))

                
                
    current_TempHys = findObject(":Oven User Interface.hysteresisVal_DIV").innerText

    current_TempHys = float(current_TempHys.strip("°F"))
    
    test.compare(current_TempHys, TempHys,"Target Reached")


def RestoreConvectionSettings():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.RESTORE DEFAULTS_button"))


def SaveConvectionSettings():
    clickButton(waitForObject(":Oven User Interface. SAVE _button_2"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.OK_button_4"))

    

def SetConvectionBIF(BIF):
    """
    Set the desired Blower Inverce Frequency
    :param EnergyLevel:
    :return: NONE
    """
    current_BIF = float(0)
    setTarget_completed = False
    count = 0
    while not (setTarget_completed) or count > 20:   
        count = count + 1
        current_BIF = findObject(":Oven User Interface.catalyticConvTempVal_DIV").simplifiedInnerText


        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        waitFor("object.exists(':Oven User Interface.blowerInvFreqVal_DIV')", 20000)
        current_BIF = findObject(":Oven User Interface.blowerInvFreqVal_DIV").innerText
        current_BIF = float(current_BIF.strip("SEC"))
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))


        
        if (current_BIF == BIF):
            setTarget_completed = True
        else:
            ce = float(current_BIF)
            int(ce)
            
            if BIF > ce:
                # arrow right
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                clickButton(waitForObject(":Oven User Interface.  _button_40"))
              
            if BIF < ce:
                # arrow Left
                clickButton(waitForObject(":Oven User Interface.  _button_41"))
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            
                
                
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.blowerInvFreqVal_DIV')", 20000)
    current_BIF = findObject(":Oven User Interface.blowerInvFreqVal_DIV").simplifiedInnerText

    current_BIF = float(current_BIF.strip("SEC"))
    test.compare(current_BIF, BIF,"Target Reached")



def GetOvenTemperature():
    """
    Gets the oven Temperature
    :return: Current temperature of oven
    """
    waitFor("object.exists(':Oven User Interface.label-preheating_DIV')", 100)

    OvenTemperature = findObject(":Oven User Interface.label-preheating_DIV").simplifiedInnerText
    return OvenTemperature


def ValidateEndOffCookingIndicators():

    waitFor("object.exists(':Oven User Interface.fanSpeedField_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.fanSpeedField_DIV").id, "fanSpeedField")
    test.compare(findObject(":Oven User Interface.fanSpeedField_DIV").innerText, "LVL01")
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.manualModalRemainingLabel_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.manualModalRemainingLabel_DIV").innerText, "00:00")
    test.compare(findObject(":Oven User Interface.manualModalRemainingLabel_DIV").id, "manualModalRemainingLabel")
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

    
def TrackCooking(CookingTemp, RF_Energy, CookingTime, FanSpeed):
    
    #update FanSpeed to new format
    try:
        if "%" in FanSpeed > -1:
            temp =FanSpeed.strip("%")
    except:
       
        temp =FanSpeed
    
    if FanSpeed >8:
        NewFanSpeed=str(int(float(temp)/100*8))
    else:
        NewFanSpeed = FanSpeed
    """
    Traks cooking progress, validates cooking parameters, waits till cooking ends
    :param CookingTemp: Desired cooking temperature
    :param RF_Energy: Desired cooking RF energy level
    :param CookingTime: Desired cooking time
    :param FanSpeed: Desired fan speed
    :return: NONE
    """
    waitFor("object.exists(':Oven User Interface.manualModalRemainingLabel_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.manualModalRemainingLabel_DIV").id, "manualModalRemainingLabel")
    snooze(5)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.fanSpeedField_DIV')", 200)
    test.compare(findObject(":Oven User Interface.fanSpeedField_DIV").id, "fanSpeedField")
    test.compare((findObject(":Oven User Interface.fanSpeedField_DIV").simplifiedInnerText).strip("LV0"), "%s" %NewFanSpeed)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.ovenTempField_DIV')", 200)
    test.compare(findObject(":Oven User Interface.ovenTempField_DIV").id, "ovenTempField")
    test.compare(findObject(":Oven User Interface.ovenTempField_DIV").simplifiedInnerText, "%s°F" % CookingTemp)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.energyLevelField_DIV')", 200)
    test.compare(findObject(":Oven User Interface.energyLevelField_DIV").id, "energyLevelField")
    test.compare(findObject(":Oven User Interface.energyLevelField_DIV").simplifiedInnerText, "LVL0%s" % RF_Energy)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    mouseClick(waitForObject(":Oven User Interface.energyLevelField_DIV"), 251, 47)
    Cooking = True
    
    while Cooking:
        TimeLeft = GetTimerValue()
        if ("00:00" == TimeLeft):
            Cooking = False
        snooze(1)

    
def ValidateCookingSettings(CookingTemp, RF_Energy, CookingTime, FanSpeed):
    """
    validates cooking settings then exits
    :param CookingTemp: Desired Cooking Temperature
    :param RF_Energy: Desired RF energy level
    :param CookingTime: Desired cooking time
    :param FanSpeed: Desired Fan Speed
    :return: NONE
    """
    waitFor("object.exists(':Oven User Interface.manualModalRemainingLabel_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.manualModalRemainingLabel_DIV").id, "manualModalRemainingLabel")
    snooze(5)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.fanSpeedField_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.fanSpeedField_DIV").id, "fanSpeedField")

    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.ovenTempField_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.ovenTempField_DIV").id, "ovenTempField")
    test.compare(findObject(":Oven User Interface.ovenTempField_DIV").simplifiedInnerText, "%s°F" % CookingTemp)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    waitFor("object.exists(':Oven User Interface.energyLevelField_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.energyLevelField_DIV").id, "energyLevelField")
    test.compare(findObject(":Oven User Interface.energyLevelField_DIV").simplifiedInnerText, "LVL0%s" % RF_Energy)
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    mouseClick(waitForObject(":Oven User Interface.energyLevelField_DIV"), 251, 47)


def GetEnergy():
    
    Energy =  findObject(":Oven User Interface.energyLevelField_DIV").simplifiedInnerText
    Energy = Energy.strip("LVL0")
    try:
        Energy = int(float(Energy))
    except:
        Energy = 0
    return Energy

def GetFanSpeed():
    """
    gets current Fan speed
    :return: Current Fan Speed
    """
    waitFor("object.exists(':Oven User Interface.fanSpeedField_DIV')", 200)
    FanSpeed = findObject(":Oven User Interface.fanSpeedField_DIV").simplifiedInnerText
    FanSpeed = FanSpeed.strip("%") 
    FanSpeed = FanSpeed.strip("LVL") 
    return int(float(FanSpeed))
    
    
def GetCookingDisplayedTemp():
    """
    gets current displayed cooking temperature
    :return: current displayed cooking temperature
    """
    try:
        CookingDisplayedTemp = findObject(":Oven User Interface.ovenTempField_DIV").simplifiedInnerText
        CookingDisplayedTemp = CookingDisplayedTemp.strip("°F")
        return float(CookingDisplayedTemp)
    except:
        CookingDisplayedTemp = ""
        return  CookingDisplayedTemp
    

def GetActualCookingDisplayedTemp():
    """
    gets actual cooking temperature
    :return: actual cooking temperature
    """
    try:
        
        doubleClick(waitForObject(":Oven User Interface.ovenTempField_DIV"), 118, 30)
        waitFor("object.exists(':Oven User Interface.ovenTempField_DIV')", 200)
    
        test.compare(findObject(":Oven User Interface.ovenTempField_DIV").domClassName, "green-glowing-label")
        CookingDisplayedTemp = findObject(":Oven User Interface.ovenTempField_DIV").simplifiedInnerText
        CookingDisplayedTemp = CookingDisplayedTemp.strip("°F")
    except:
        CookingDisplayedTemp = ""
    return CookingDisplayedTemp


def GetTimerValueSeconds():
    """
    Gets current timer value
    :return: current timer value
    """
    TimerValue = ""
    try:
        TimerValue = findObject(":Oven User Interface.manualModalRemainingLabel_DIV").innerText
    except:
        TimerValue = "NA"
        
    mc, sc = TimerValue.split(":")
  
           
    TimerValue =  60 * float (mc) + float(sc) 
    return TimerValue


def ConVertTimerToSeconds(TimerValue):
    #MessageBox("timer", TimerValue.count(":") , 1)
    if TimerValue.count(":") > 2:
        hc, mc, sc = TimerValue.split(":")
             
        ct = float(hc) * 60 * 60 + 60 * float (mc) + float(sc) 
        TimerValue  = ct
    
    return TimerValue
    
def GetTimerValue():
    """
    Gets current timer value
    :return: current timer value
    """
    TimerValue = ""
    try:
        TimerValue = findObject(":Oven User Interface.manualModalRemainingLabel_DIV").innerText
    except:
        TimerValue = "NA"
    return TimerValue
    
    
def SetFanSpeed(desiredValue):
    if desiredValue >8:
        AdjusteddesiredValue =  float((float(desiredValue)/100*8))
    else:
        AdjusteddesiredValue= desiredValue
    """
    Sets Desired Fam Speed
    :param desiredValue: Fan Speed
    :return: NONE
    """
    current_value = ""
    setTarget_completed = False
    
    while not (setTarget_completed):   
        current_value = findObject(":Oven User Interface.progress-label-fan_DIV").innerText

        current_value = current_value.strip("%")    
        current_value = float(current_value)
       #MessageBox("Action", 'Fan SpeedSet %s  = %s ' %(current_value,AdjusteddesiredValue), 1)
        if (int (current_value) == AdjusteddesiredValue):
            setTarget_completed = True
        else:
            
            if AdjusteddesiredValue > current_value:
                clickButton(waitForObject(":Oven User Interface.  _button_16"))
            if AdjusteddesiredValue < current_value:
                clickButton(waitForObject(":Oven User Interface.  _button_23"))
    
    
def NewTimeFormat(CookingTime):
        if CookingTime.count(":") >1:  
            hc,mc,sc = CookingTime.split(":")  
            
            hours=__builtin__.int(hc)
            minutes = __builtin__.int(mc)
            #seconds=__builtin__.int(sc)
            
            newMinutes= ((hours*60)  + minutes)
            updatedMinutes = "%02d" %newMinutes
            
            NewCookingTime = "%s:%s" %(updatedMinutes,sc)
        
        else:
            NewCookingTime= CookingTime
        
        return NewCookingTime
    
    
def SetCookingTime(CookingTime):

        
    """
    Set cooking time
    :param CookingTime: Desired cooking time
    :return: NONE
    """
    current_value = ""
    setTarget_completed = False
    
    while not (setTarget_completed):   
        current_value = findObject(":Oven User Interface.progress-label-time_DIV").innerText
        #AdjustMent = current_value.split(":")

        if CookingTime.count(":") > 1:
            mc,sc = current_value.split(":")
            ht,mt,st = CookingTime.split(":")
               
            ct =  60 * float (mc) + float(sc)  
            tt = float(ht) * 60 * 60 + 60 * float (mt) + float(st)     
        else:
            mc, sc = current_value.split(":")
            mt, st = CookingTime.split(":")
               
            ct = 60 * float (mc) + float(sc)  
            tt = 60 * float (mt) + float(st) 
        
        current_value = ct
        desiredValue = tt
        if (current_value == desiredValue):
            setTarget_completed = True
        else:
            
            if desiredValue > current_value:
                clickButton(waitForObject(":Oven User Interface.  _button_3"))
            if desiredValue < current_value:
                clickButton(waitForObject(":Oven User Interface.  _button_4"))


def WaitTillCookingEnd():
    """
    Waits till the cooking timer ends
    :return: NONE
    """
    waitFor("object.exists(':Oven User Interface.manualModalRemainingLabel_DIV')", 2000)
    test.compare(waitForObject(":Oven User Interface.manualModalRemainingLabel_DIV").innerText, "00:00:00")
	
    
def WaitTillOvenPreheated():
    """
    Waits unitll the oven reaches the preheat temperature
    :return: NONE
    """
    preheat_completed = False
    waitForObject(":Oven User Interface.label-preheating_DIV", 2000)

    while not (preheat_completed):   
        current_Status = findObject(":Oven User Interface.label-preheating_DIV").domClassName
        if (current_Status == "col-xs-12 text-center yellow-glowing-label"):
             preheat_completed = True
        else:     
            snooze(2)
            
            
def CheckOvenReady(TargetTemperature = 0):
    """
    Waits till the oven reaches preheat temperature then exits
    :param TargetTemperature: Desired preheat temperature
    :return:
    """
    snooze(10)
    preheat_completed = False
    TargetTemperatureReadched = False
    waitForObject(":Oven User Interface.label-preheating_DIV", 200000)

    while not (preheat_completed):   

        current_Status =findObject(":Oven User Interface.label-preheating_DIV").domClassName

        
        if (current_Status ==  "col-xs-12 text-center blink-item yellow-glowing-label"):
             preheat_completed = True
             
             if TargetTemperature != 0:
                 if str(TargetTemperature) in str(GetOvenTemperature()) > -1:
                     TargetTemperatureReadched = True
                 
        else:     
            snooze(2)
    if TargetTemperature != 0:
        test.compare(TargetTemperatureReadched, True, "Target Temperature Reached")
        
        
def ClearSnifferLogs():
    try:
        os.remove("Sniffer.msblog")
    except:
        found = False
    try:
        os.remove("testA.msblog")
    except:
        found = False
    try:    
        os.remove("testAB.msblog")
    except:
        found = False
    try:
        os.remove("testB.msblog")
    except:
        found = False
        
        
def PauseCooking():
    """
    Presses the pause button
    :return: NONE
    """
    
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _submit"))       
 
 
def ResumeCooking():
    """
    presses teh resume cooking button
    :return:
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _submit"))
    
    
def StopRecipeCooking():
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_6"))


def StopManualCooking():
    """
    Presses the stop cooking button
    :return: NONE
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_6"))
    snooze(1)
   
        
def StopCooking():
    """
    Presses the stop cooking button
    :return: NONE
    """
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_6"))
    snooze(1)
    try:
        clickButton(waitForObject(":Oven User Interface.OK_button"))   
        snooze(10) 
    except:
        snooze(10)
    
def ValidateFanSpeed(FanSpeed):
    """
    Check fan speed
    :param FanSpeed: Desired fan speed
    :return: NONE
    """
    # Fan Speed
    waitFor("object.exists(':Oven User Interface.fanSpeedField_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.fanSpeedField_DIV").id, "fanSpeedField")
    test.compare((findObject(":Oven User Interface.fanSpeedField_DIV").simplifiedInnerText).strip("%"), "%s" % FanSpeed)

def AddMoreCookingTime():


    startBrowser("http://192.168.10.19:13321/ibex/")
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.  _button_30"))
    clickButton(waitForObject(":Oven User Interface.OK_button_3"))
    clickButton(waitForObject(":Oven User Interface. START _button_2"))
    clickButton(waitForObject(":Oven User Interface.  _button_5"))
    clickButton(waitForObject(":Oven User Interface.60 secs_button"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

    
    
    
def AddRecipeStep(CookingTemp, RF_Energy, CookingTime, FanSpeed):
    """
    Adds a recipe Step
    :param CookingTemp: Desired cooking temperature
    :param RF_Energy: Desired RF energy level
    :param CookingTime: Desired cooking temperature
    :param FanSpeed: Desired fan speed
    :return: NONE
    """
    clickButton(waitForObject(":Oven User Interface.  _button_18"))

    snooze(1)
    SetCookingTemp(CookingTemp)
    SetRFEnergy(RF_Energy)
    SetFanSpeed(FanSpeed)
    SetCookingTime(CookingTime)    
    

def ValidateRecipeStep(CookingTemp, RF_Energy, CookingTime, FanSpeed, RecipeName, Step=1):
    
    CookingTime = NewTimeFormat(CookingTime)
    #test.compare(CookingTime,"0000","%s%s" %(CookingTime,0))
    """
    Validates the recipe gsteps
    :param CookingTemp: Desired cooking temperature
    :param RF_Energy: Desired RF energy level
    :param CookingTime: Desired cooking temperature
    :param FanSpeed: Desired Fan Speed
    :param RecipeName: Recipe name
    :param Step: Recipe step number
    :return: NONE
    """
    # Read recipe
    RecipeFound = False
    waitFor("object.exists(':Oven User Interface.recipe-list-item-0_DIV')", 1000)
    recipe0 = findObject(":Oven User Interface.recipe-list-item-0_DIV").simplifiedInnerText
    
    if recipe0 == RecipeName:
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _button_26"))
        
        # select recipe to edit
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.  _button_19"))
    
        # validate all fields
        count = 0
        snooze(1)

        for iteration in range(1, 20):
                   
            currentStep = findObject(":Oven User Interface.recipe-step-label_DIV").simplifiedInnerText
            currentStep = currentStep.strip("STEP ")
            currentStep = currentStep.split("/")
              
            if (int (Step) == int(float(currentStep[0]))):
                         
                test.compare(findObject(":Oven User Interface.progress-label-temp_DIV").simplifiedInnerText, "%s°F" % CookingTemp)
    
                waitFor("object.exists(':Oven User Interface.progress-label-rflevel_DIV')", 20000)
                test.compare(findObject(":Oven User Interface.progress-label-rflevel_DIV").id, "progress-label-rflevel")
                test.compare(findObject(":Oven User Interface.progress-label-rflevel_DIV").simplifiedInnerText, "%s" % RF_Energy)
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))

                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                waitFor("object.exists(':Oven User Interface.progress-label-time_DIV')", 20000)
                test.compare(findObject(":Oven User Interface.progress-label-time_DIV").id, "progress-label-time")
                test.compare(findObject(":Oven User Interface.progress-label-time_DIV").simplifiedInnerText, "%s" %CookingTime)
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
                RecipeFound = True       
            else:
                # got to next step
                clickButton(waitForObject(":Oven User Interface.  _button_27"))
                activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    ExitRecipeEditMenu()


def ExitRecipeEditMenu():   
    """
    Exits RecipeMenu
    RETURN : NONE 
    """

    clickButton(waitForObject(":Oven User Interface.  _button_34"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    
    # Exit with out saving
    clickButton(waitForObject(":Oven User Interface.EXIT_button"))
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    
    
def SelectRecipe(RecipeName):
    """
    This module will select the Recipe by name from the visible list
    :param RecipeName:
    :return: NONE
    """

    for recipe in range(100):

        try:
            CurrentRecipeName = findObject(":Oven User Interface.recipe-list-item-%s_DIV" % recipe).simplifiedInnerText
            if RecipeName == CurrentRecipeName:
                mouseClick(waitForObject(findObject(":Oven User Interface.recipe-list-item-%s_DIV" % recipe)))
                test.compare(True, True, 'SelectedRecipe')
            
                break
        except:
            test.compare(False, True, 'SelectedRecipe')
            break


def CreateRecipe(CookingTemp, RF_Energy, CookingTime, FanSpeed):
    """
    Creates the recipe with desired cookingsettngs
    :param CookingTemp: Desired cookig temperature
    :param RF_Energy: Desired RF energy level
    :param CookingTime: Desired cooking time
    :param FanSpeed: Desired Fan Speed
    :return: NONE
    """
    if  not RecipeEmpty():
        clickButton(waitForObject(":Oven User Interface.  _button_17"))
        snooze(1)
        SetCookingTemp(CookingTemp)
        SetRFEnergy(RF_Energy)
        SetFanSpeed(FanSpeed)
        SetCookingTime(CookingTime)    
    else:
        clickButton(waitForObject(":Oven User Interface.  _button_17"))
        snooze(1)
        SetCookingTemp(CookingTemp)
        SetRFEnergy(RF_Energy)
        SetFanSpeed(FanSpeed)
        SetCookingTime(CookingTime)  
    
def SaveRecipe(RecipeName):
    """
    Saves recipe
    :param RecipeName: Name of recipe
    :return: NONE
    """
    clickButton(waitForObject(":Oven User Interface. SAVE _button"))
    setFocus(waitForObject(":Oven User Interface.recipeName_text"))
    setText(waitForObject(":Oven User Interface.recipeName_text"), "%s" % RecipeName)
    clickButton(waitForObject(":Oven User Interface.  OK _button_3"))
    snooze(1)


def CheckOvenToHotPopUp():
    """
    Checks for oven to Hot pop up window and exits window
    :return: NONE
    
    """
    PopUpFound = True
    try:
        """
        waitFor("object.exists(':Oven User Interface.               --°F          The oven is too hot, open the doorto cool down at even fastest rate.                        Your recipe may be affected by the higher temperaturedo you want to start anyway?        Main message second          CANCEL      MIDDLE       START     _DIV')", 20000)
        test.compare(findObject(":Oven User Interface.               --°F          The oven is too hot, open the doorto cool down at even fastest rate.                        Your recipe may be affected by the higher temperaturedo you want to start anyway?        Main message second          CANCEL      MIDDLE       START     _DIV").domPath, "DOCUMENT.HTML1.BODY1.DIV3.DIV1.DIV1.DIV1")
        test.compare(findObject(":Oven User Interface.               --°F          The oven is too hot, open the doorto cool down at even fastest rate.                        Your recipe may be affected by the higher temperaturedo you want to start anyway?        Main message second          CANCEL      MIDDLE       START     _DIV").simplifiedInnerText, "--°F The oven is too hot, open the doorto cool down at even fastest rate. Your recipe may be affected by the higher temperaturedo you want to start anyway? Main message second CANCEL MIDDLE START")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        clickButton(waitForObject(":Oven User Interface.CANCEL_button"))
    
        """

        waitFor("object.exists(':Oven User Interface.The oven is too hot, open the door to cool down at even fastest rate. Your recipe may be affected by the higher temperature do you want to start anyway? CANCEL START_DIV')", 20000)
        test.compare(findObject(":Oven User Interface.The oven is too hot, open the door to cool down at even fastest rate. Your recipe may be affected by the higher temperature do you want to start anyway? CANCEL START_DIV").simplifiedInnerText, "The oven is too hot, open the door to cool down at even fastest rate. Your recipe may be affected by the higher temperature do you want to start anyway? CANCEL START")
        activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
        PopUpFound = True
    except:
        PopUpFound = False
        
    if not PopUpFound:
        try:    
        
            # ovne to hot recipe message
            waitFor("object.exists(':Oven User Interface.The oven is too hot, open the doorto cool down at even fastest rate._DIV')", 20000)
            test.compare(findObject(":Oven User Interface.The oven is too hot, open the doorto cool down at even fastest rate._DIV").innerText, "The oven is too hot, open the doorto cool down at even fastest rate.")
            PopUpFound =True
        except:
            PopUpFound = False
            
        if not PopUpFound:
            test.compare(findObject(":Oven User Interface.Oven is too hot_DIV").innerText, "Oven is too hot")
      
       
       
def ValidateServiceScreen(UserLevel = 3):
    try:
        if UserLevel == 4:
            waitFor("object.exists(':Oven User Interface.           BACK                                                                  $(document).ready(function() {  $(\\'#header-bar\\').removeClass(\\'header-bar-fixed\\');  $(\\'#mainContainer\\').removeClass(\\'main-container-fixed\\');  resetAll();  attachServiceActions();  });    _DIV')", 20000)
            test.compare(findObject(":Oven User Interface.           BACK                                                                  $(document).ready(function() {  $('#header-bar').removeClass('header-bar-fixed');  $('#mainContainer').removeClass('main-container-fixed');  resetAll();  attachServiceActions();  });    _DIV").simplifiedInnerText, "BACK $(document).ready(function() { $('#header-bar').removeClass('header-bar-fixed'); $('#mainContainer').removeClass('main-container-fixed'); resetAll(); attachServiceActions(); });")
            waitFor("object.exists(':Oven User Interface.BACK_button')", 20000)
            test.compare(findObject(":Oven User Interface.BACK_button").visible, True)
            test.compare(findObject(":Oven User Interface.BACK_button").id, "backToUI")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.  _button_9')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_9").domClassName, "btn btn-link btn-lg")
            test.compare(findObject(":Oven User Interface.  _button_9").visible, True)
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.  _button_10')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_10").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV2.BUTTON1")
            test.compare(findObject(":Oven User Interface.  _button_10").visible, True)
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.  _button_11')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_11").visible, True)
            test.compare(findObject(":Oven User Interface.  _button_11").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.BUTTON1")
            waitFor("object.exists(':Oven User Interface.  _button_12')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_12").visible, True)
            test.compare(findObject(":Oven User Interface.  _button_12").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV4.DIV1.BUTTON1")
            waitFor("object.exists(':Oven User Interface.  _button_13')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_13").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV4.DIV2.BUTTON1")
            test.compare(findObject(":Oven User Interface.  _button_13").visible, True)
            waitFor("object.exists(':Oven User Interface.  _button_14')", 20000)
            test.compare(findObject(":Oven User Interface.  _button_14").visible, True)
            test.compare(findObject(":Oven User Interface.  _button_14").domPath, "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV4.DIV3.BUTTON1")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            clickButton(waitForObject(":Oven User Interface.BACK_button")   )  
        if UserLevel  == 3:
            waitFor("object.exists(':Oven User Interface.servicePeripherals_DIV')", 20000)
            test.compare(findObject(":Oven User Interface.servicePeripherals_DIV").id, "servicePeripherals")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
            waitFor("object.exists(':Oven User Interface.ovenSelfTestAndLog_DIV')", 20000)
            test.compare(findObject(":Oven User Interface.ovenSelfTestAndLog_DIV").id, "ovenSelfTestAndLog")
            waitFor("object.exists(':Oven User Interface.versAndFirmwUpdate_DIV')", 20000)
            test.compare(findObject(":Oven User Interface.versAndFirmwUpdate_DIV").id, "versAndFirmwUpdate")
            waitFor("object.exists(':Oven User Interface.BACK_button')", 20000)
            test.compare(findObject(":Oven User Interface.BACK_button").id, "backToUI")
            activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))            
            
    except:
            test.compare(True, False, "Test Failed")
            
            
def CheckOvenToColdPopUp():
    """
    Checks for oven to cold pop up window and exits window
    :return:
    """
    snooze(2)


    """
    waitFor("object.exists(':Oven User Interface.               --°F          The oven is notfinished preheating.                        Your recipe may be affected by the lower temperaturedo you want to wait or start?        Main message second           WAIT      MIDDLE       START     _DIV')", 20000)
    # test.compare(findObject(":Oven User Interface.               --°F          The oven is notfinished preheating.                        Your recipe may be affected by the lower temperaturedo you want to wait or start?        Main message second           WAIT      MIDDLE       START     _DIV").domPath, "DOCUMENT.HTML1.BODY1.DIV3.DIV1.DIV1.DIV1")
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.WAIT_button"))
    """


    waitFor("object.exists(':Oven User Interface.Oven is too cold_DIV')", 20000)
    test.compare(findObject(":Oven User Interface.Oven is too cold_DIV").innerText, "Oven is too cold")
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    
    # exit menu by hitting back button
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface.   BACK _button_3"))



    
    
    
def EndSession():
    """
    closes the browser
    :return: NONE
    """
    snooze(1)
    closeWindow(":[Window]")
    
    
def CheckCurrentMenu():
    """
    Identifies current visible menu
    Return: Returns current menu
    """
    try:
        if ((findObject(":Oven User Interface.header-label_DIV").innerText) in  "MY CREATIONS" > -1):
            return "Recipe Menu"
    except:
        found = False
    try:
        if ((findObject(":Oven User Interface.Bakery_AREA_2").id) in "startManual-bakery" > -1):
            return "Home Menu"
      
    except:
        found = False
    
    return "Not Found"
    

def ClickWaitInOvenTooColdPopups():
    clickButton(waitForObject(":Oven User Interface.WAIT_button"))


def ClickStartInOvenTooColdPopups():
    mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV3.DIV1.DIV1.DIV1.DIV7.DIV3.BUTTON1.SPAN1"), 20, 19)
    
    
def ClickCancelInOvenTooHotPopups():
    clickButton(waitForObject(":Oven User Interface.CANCEL_button"))
    
    
def ClickStartInOvenTooHotPopups():
    mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV3.DIV1.DIV1.DIV1.DIV7.DIV3.BUTTON1.SPAN1"), 50, 28)
        
def GeatBarValue(BarName):
    if BarName == "CATALYTIC":
        waitFor("object.exists(':Oven User Interface.catalyticVal_DIV')", 20000)
        CurrentValue = findObject(":Oven User Interface.catalyticVal_DIV").innerText
        
    return CurrentValue


def GetBarLimits(BarName,Action):
 
    PrevioustValue = 0
    if Action =="HighLimit":
        for index in range(200):
            CurrentValue = GeatBarValue("CATALYTIC")
            if not(CurrentValue == PrevioustValue):
                if BarName == "CATALYTIC":
                    #right mouse clic
                    mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV2.DIV1.DIV1.DIV2.DIV3.BUTTON1.SPAN1"), 7, 15)
                PrevioustValue = CurrentValue
            else:
                break
    if Action =="LowLimit":
        for index in range(200):
            CurrentValue = GeatBarValue("CATALYTIC")
            if not(CurrentValue == PrevioustValue):
                if BarName == "CATALYTIC":
                    #Left mouse Click
                    mouseClick(waitForObject(":{title='Oven User Interface' type='BrowserTab'}.DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV2.DIV1.DIV1.DIV2.DIV1.BUTTON1.SPAN1"), 9, 12)
                PrevioustValue = CurrentValue
            else:
                break
    return CurrentValue
