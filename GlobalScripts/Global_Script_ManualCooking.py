       
def StartOvenManualCooking():
    """
    Presses Start Button for manual cooking
    :return: NONE
"""
    activateBrowserTab(waitForObject(":Oven User Interface_BrowserTab"))
    clickButton(waitForObject(":Oven User Interface. START _button"))
  
    