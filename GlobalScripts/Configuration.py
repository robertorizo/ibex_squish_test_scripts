# Configuration Settings for tests

#User level supported 1,2,4
import __builtin__

RunningOnSimulator = False

#UserLevel = 1   

TEMPSETTLINGTIME = 15
HIGHLIMITPAGETRANSITION = 2

LOWESTTEMPERATURE = 200
HIGHTESTTEMPERATURE = 450

LOWESTTEMPERATURE_C = 95
HIGHTESTTEMPERATURE_C = 165


USERLEVELPSW1 = 1    #Operator
USERLEVELPSW2 = 1051 #Cheff
USERLEVELPSW3 = 6731 #Service
USERLEVELPSW4 = 1992 #Eng

RFSettingsLowLimit = 0
RFSettingsHighLimit = 5
RFSettingsStepSize = 1

CookingTimerDeltaLimit = .5
CatalyticHighLimit = 600
CatalyticLowLimit = 200

TestTimeOut = 20*60  # min * seconds
RecipeLimit = 100
RecipeStepLimit = 20

RecipeCountLimit = 1000
#url = "http://192.168.10.31:13321/ibex/"
#url = "http:// 192.168.10.32:13321/ibex/"
#url = "http://192.168.10.19:13321/ibex/"
#url = "http://192.168.10.32:13321/ibex/"
url = "http://192.168.10.19:13321/ibex/"

DEFAULTPREHEATTEMPERATURE = 300
#DEFAULTPREHEATTEMPERATURE = 200